# Middleware Request History ideal endpoint structures

<img src="https://drive.google.com/uc?export=view&id=170wOuVepj01Hzei17-2oexqdcA4lcXfN" width=100%/>

## HTTP status and error codes for JSON
The following provides reference information regarding the proposed JSON:API response structures relating to status codes and error messages from the API.

### Error Response Format

The following is an example of an error response you receive if you try to list the buckets for a project but do not provide an authorization header.
```
// 401 Unauthorized

{
"error": {
 "errors": [
  {
   "domain": "global",
   "reason": "required",
   "message": "Login Required",
   "locationType": "header",
   "location": "Authorization"
  }
 ],
 "code": 401,
 "message": "Login Required"
 }
}
```